<h3>Gestor de productos <small>Alta de producto</small></h3>
<hr>

<?php if($_SESSION['conectado']){ ?>

<form action="index.php?pag=insercion.php" method="post" enctype="multipart/form-data">
	
	<input class="form-control" type="text" name="nombreProducto" placeholder="Escribe el nombre de producto"><br>
	
	<input class="form-control" type="text" name="precioProducto" placeholder="Escribe el precio de producto"><br>

	<input class="form-control" type="text" name="cantidadProducto" placeholder="Escribe la cantidad de producto"><br>

	<textarea class="form-control" name="descripcionProducto" cols="30" rows="10"></textarea><br>

	<input class="form-control" type="file" name="imagenProducto"><br>

	<select name="idCat" class="form-control">
		<?php  
		$sqlCat="SELECT * FROM categorias ORDER BY nombreCat ASC";
		$consultaCat=$conexion->query($sqlCat);
		while($filaCat=$consultaCat->fetch_array()){
			?>
			<option value="<?php echo $filaCat['idCat'];?>">
				<?php echo $filaCat['nombreCat'];?>
			</option>
			<?php
		}
		?>
	</select><br>

	<label for="etiquetas">Etiquetas de producto:</label><br>
	<?php 
 
		$sqlEti="SELECT * FROM etiquetas ORDER BY nombreEti ASC";
		$consultaEti=$conexion->query($sqlEti);
		while($filaEti=$consultaEti->fetch_array()){
			?>
			<input type="checkbox" value="<?php echo $filaEti['idEti'];?>" name="etiquetas[]">
				<?php echo $filaEti['nombreEti'];?>
			<br>
			<?php
		}

	?>


	<input class="form-control btn-aquamarine" type="submit" name="insertar" value="insertar">

</form>
<?php 
	}else{
		echo 'NO TIENES PERMISO PARA ESTAR AQUI... LISTO...';
	} 
?>