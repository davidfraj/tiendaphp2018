--
-- Estructura de tabla para la tabla `etiquetas`
--

CREATE TABLE `etiquetas` (
  `idEti` int(11) NOT NULL,
  `nombreEti` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `etiquetas`
--

INSERT INTO `etiquetas` (`idEti`, `nombreEti`) VALUES
(1, 'hardware'),
(2, 'software'),
(3, 'ratones'),
(4, 'teclados'),
(5, 'torres'),
(6, 'varios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etiquetasproductos`
--

CREATE TABLE `etiquetasproductos` (
  `idEtiPro` int(11) NOT NULL,
  `idEti` int(11) NOT NULL,
  `idPro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `etiquetasproductos`
--

INSERT INTO `etiquetasproductos` (`idEtiPro`, `idEti`, `idPro`) VALUES
(6, 2, 12),
(7, 4, 12),
(8, 6, 12);

-- --------------------------------------------------------

--
-- Indices de la tabla `etiquetas`
--
ALTER TABLE `etiquetas`
  ADD PRIMARY KEY (`idEti`);

--
-- Indices de la tabla `etiquetasproductos`
--
ALTER TABLE `etiquetasproductos`
  ADD PRIMARY KEY (`idEtiPro`);
